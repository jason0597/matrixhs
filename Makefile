SOURCES			:= source
INCLUDES		:= include
BUILD			:= build
TARGET			:= $(notdir $(CURDIR))

CCX				:= g++
LD				:= g++
CCXFLAGS 		:= -Wall -Wextra -Werror -g -Og -c -std=c++17 -I $(INCLUDES)

SOURCEFILES		:= $(wildcard $(SOURCES)/*.cpp)
BASEFILES		:= $(notdir $(basename $(SOURCEFILES)))
OFILES			:= $(addprefix build/, $(addsuffix .o, $(BASEFILES)))

$(TARGET): $(BUILD) $(OFILES)
	@echo [LINKING]
	@$(LD) $(OFILES) -o $(BUILD)/$(TARGET)

$(BUILD):
	@mkdir -p $(BUILD)
	@echo [CCFXLAGS]: $(CCXFLAGS)

$(BUILD)/%.o: source/%.cpp
	@echo [CC] $<
	@$(CC) $(CCXFLAGS) -o $@ $<

clean:
	@echo Cleaning...
	@rm -rf $(BUILD) $(BUILD)/$(TARGET)
